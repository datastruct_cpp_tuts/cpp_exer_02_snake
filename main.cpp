#include "conio.h"
#include <chrono>
#include <cstddef>
#include <iostream>
#include <thread>

/*
This main file is just an example of conio.h usages.
As you can see kbhit() and getch() methods allow you to read
the key that was pressed by the user.
std::systeam("clear") on the other hand lets you to clear everything
that was printed before.
*/

#include <vector>

#define MAX_X 50
#define MAX_Y 25

enum CordType { Cookie, Snake, Space };

struct Cord {
  int x = -1;
  int y = -1;
};

bool check_fail(std::vector<Cord> &snake) {
  Cord &head = snake.back();
  for (auto i = 0; i < snake.size() - 1; ++i) {
    if (head.x == snake[i].x && head.y == snake[i].y) {
      std::system("clear");
      std::cout << "GAME OVER" << '\n';
      std::cout << head.x << ' ' << snake[i].x << ' ' << head.y << ' '
                << snake[i].y << '\n';
      return true;
    }
  }
  if (head.x > MAX_X || head.y > MAX_Y || head.x < 0 || head.y < 0) {
    std::cout << "MAX" << '\n';
    return true;
  }
  return false;
}

Cord gen_cookie(std::vector<Cord> &snake) {
  Cord cookie = {.x = (rand() % MAX_X) + 1, .y = (rand() % MAX_Y) + 1};
  for (auto snake_c : snake) {
    if (cookie.x == snake_c.x && cookie.y == snake_c.y) {
      return gen_cookie(snake);
    }
  }
  return cookie;
}

void draw_playground(std::vector<Cord> &snake, Cord &cookie) {
  CordType cord_type = Space;
  for (auto i = 0; i < MAX_Y + 1; ++i) {
    for (auto j = 0; j < MAX_X + 1; ++j) {
      cord_type = Space;
      for (auto snake_c : snake) {
        if (j == snake_c.x && i == snake_c.y) {
          cord_type = Snake;
        }
      }
      if (j == cookie.x && i == cookie.y) {
        cord_type = Cookie;
      }
      char ch = ' ';
      if (cord_type == Snake) {
        ch = '#';
      } else if (cord_type == Cookie) {
        ch = '@';
      }
      std::cout << ch;
    }
    std::cout << '^';
    std::cout << '\n';
  }
  for (auto i = 0; i < MAX_X + 2; ++i) {
    std::cout << '^';
  }
  std::cout << '\n';
}

void move_snake(std::vector<Cord> &snake, char key) {

  size_t snake_size = snake.size();
  size_t snake_last_index = snake_size - 1;

  switch (key) {
  case 'w':
    for (auto i = 0; i < snake_size; ++i) {
      if (i != snake_last_index) {
        snake[i] = snake[i + 1];
      } else {
        snake[i].y -= 1;
      }
    }
    break;
  case 'a':
    for (auto i = 0; i < snake_size; ++i) {
      if (i != snake_last_index) {
        snake[i] = snake[i + 1];
      } else {
        snake[i].x -= 1;
      }
    }
    break;
  case 's':
    for (auto i = 0; i < snake_size; ++i) {
      if (i != snake_last_index) {
        snake[i] = snake[i + 1];
      } else {
        snake[i].y += 1;
      }
    }
    break;
  default:
    for (auto i = 0; i < snake_size; ++i) {
      if (i != snake_last_index) {
        snake[i] = snake[i + 1];
      } else {
        snake[i].x += 1;
      }
    }
    break;
  }
}

void eat_cookie(std::vector<Cord> &snake, Cord &cookie, Cord &last_tail) {
  Cord &head = snake.back();
  if (head.x == cookie.x && head.y == cookie.y) {
    cookie.x = -1;
    cookie.y = -1;
    snake.insert(snake.begin(), last_tail);
  }
}

int main() {
  srand((unsigned)time(0));

  std::vector<Cord> snake = {{
                                 .x = 5,
                                 .y = 5,
                             },
                             {
                                 .x = 6,
                                 .y = 5,
                             },
                             {
                                 .x = 7,
                                 .y = 5,
                             }};

  Cord last_tail = snake.front();
  Cord cookie{.x = -1, .y = -1};
  Cord &head = snake.back();
  char key = 'd';

  using namespace std::chrono_literals;

  std::cout << "Starting..." << '\n';
  std::this_thread::sleep_for(1000ms);
  std::system("clear");

  while (true) {
    // gen new cookie if last was eaten
    if (cookie.x == -1) {
      cookie = gen_cookie(snake);
    }

    // draw the playground
    draw_playground(snake, cookie);

    // save some data
    last_tail = snake.front();

    // get key and react
    if (kbhit())
      key = getch();
    move_snake(snake, key);

    // eat the cookie
    eat_cookie(snake, cookie, last_tail);

    // GAME OVER
    if (check_fail(snake)) {
      return 0;
    }

    std::this_thread::sleep_for(200ms);

    std::system("clear");
  }

  return 0;
}
